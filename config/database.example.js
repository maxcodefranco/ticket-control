const fs = require('fs');

module.exports = {
  development: {
    username: null,
    password: null,
    database: null,
    host: null,
    dialect: null,
  },
  test: {
    username: null,
    password: null,
    database: null,
    host: null,
    dialect: null,
  },
  production: {
    username: null,
    password: null,
    database: null,
    host: null,
    dialect: null
  }
};