const express = require('express');
const app = express();
const config = require('../config/config');
const util = require('util');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

exports.app = app;

//CONFIGURE

app.set('view engine','ejs');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(express.static(__dirname + '/../pulbic'));



app.get('/',(req,res)=>{
    res.json("ok")
})

fs.readdirSync(path.resolve(__dirname+'/../routes')).forEach((file)=>{
    const pathFull = path.resolve(__dirname+'/../routes/'+file);

    if(!fs.lstatSync(pathFull).isFile()) return;
    var _route = require(pathFull);
    app.use('/'+_route.alias,_route.routes);
})



app.listen(config,()=>{
    console.log(util.format('Servidor iniciado na porta %s',config.port))
})